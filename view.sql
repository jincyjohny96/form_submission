CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `new_db`.`vw_usen_list` AS
    SELECT 
        `ul`.`user_id` AS `user_id`,
        `ul`.`full_name` AS `full_name`,
        `e`.`enquiry_id` AS `enquiry_id`
    FROM
        (`new_db`.`tbl_user_list` `ul`
        LEFT JOIN `new_db`.`tbl_enquiry` `e` ON (`ul`.`user_id` = `e`.`client_id`))